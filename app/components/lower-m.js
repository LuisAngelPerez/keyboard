
import Component from '@ember/component';
import Ember from 'ember';
import { inject as service} from '@ember/service';

export default Component.extend({
    keyboard: service('ember-cordova/keyboard'),
    keyboardIsShowing: false,

    didInsertElement() {
        this._super();

        this.get('keyboard').on('keyboardDidShow', this.keyboardDidShow);
        this.get('keyboard').on('keyboardDidHide', this.keyboardDidHide);
    },

    willDestroyElement() {
        this.get('keyboard').off('keyboardDidShow', this.keyboardDidShow);
        this.get('keyboard').off('keyboardDidHide', this.keyboardDidHide);

        this._super();
    },

    keyboardDidShow() {
        this.set('keyboardIsShowing', true);
    },

    keyboardDidHide() {
        this.set('keyboardIsShowing', false);
    }
});